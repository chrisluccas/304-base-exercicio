FROM openjdk:8

ADD /target /target

WORKDIR /target/

CMD [ "java", "-Xmx512m", "-jar", "Api-Investimentos-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080